# Python program to explain os.getenv() method 
	
# importing os module 
import os 
from dotenv import load_dotenv

# Get the value of 'HOME' 
# environment variable 
load_dotenv()
key = 'DISCORD_TOKEN'
value = os.getenv(key) 

# Print the value of 'HOME' 
# environment variable 
print("Value of 'HOME' environment variable :", value) 
