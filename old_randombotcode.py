def outputRules(text):
	game = text.lower()
	filename = "RULES.txt"
	f = ''
	folderpathname = ''
	for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
	if folderpathname != '':
		for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if filename.casefold() in os.path.basename(file).casefold():
					f = file
					break
		if f != '':
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			return(Lines)
		else:
			return("Couldn't find any RULES for this game")
	else:
		return("Couldn't find any RULES for this game")
	
	
def addPunishmentSuggestions(text):
	fileName = 'PunishmentSuggestions.txt'
	FoundFile = False
	for root, dirs, files in os.walk("."):
		for file in files:
			if fileName.casefold() in os.path.basename(file).casefold():
				filename = file
				FoundFile = True
				break
	if FoundFile == False:
		print("Created File")
		newfile = open(fileName, 'a').close()
	newfile = open(fileName, 'a')
	newfile.write(text + '\n')
	print("Updated")
	newfile.close()
	newfile = open(fileName, 'r')
	Lines = newfile.readlines()
	newfile.close()
	return Lines
		
def viewPunishmentSuggestions():
	fileName = 'PunishmentSuggestions.txt'
	FoundFile = False
	for root, dirs, files in os.walk("."):
		for file in files:
			if fileName.casefold() in os.path.basename(file).casefold():
				filename = file
				FoundFile = True
				break
	if FoundFile == False:
		print("Created File")
		newfile = open(fileName, 'a').close()
	newfile = open(fileName, 'r')
	Lines = newfile.readlines()
	newfile.close()
	return Lines

def getPunishmentSuggestionsWheelLink():
	fileName = 'wheelofPunishmentsURL.txt'
	FoundFile = False
	for root, dirs, files in os.walk("."):
		for file in files:
			if fileName.casefold() in os.path.basename(file).casefold():
				filename = file
				FoundFile = True
				break
	if FoundFile == False:
		print("Created File")
		newfile = open(fileName, 'a').close()
	newfile = open(fileName, 'r')
	Lines = newfile.readlines()
	newfile.close()
	return Lines	
		
def updatePersonRefSheetNum(newgame, participantName, newnum=1):
	game = newgame.lower()
	f = ""; 
	folderpathname = ''
	for root, dirs, files in os.walk("."):
		for dir in dirs:
			if game.casefold() in os.path.basename(dir).casefold():
				folderpathname = dir
				break
	fileName = 'LookUpSheetCounter'	
	pathfile = './' + folderpathname + '/' + fileName + '.csv'
	if folderpathname == '':
		return "Could not find anything"
	else:
		foundFile = False
		for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if fileName.casefold() in os.path.basename(file).casefold():
					print(os.path.basename(fileName).casefold())
					foundFile = True
					break
		if foundFile == False:
			return ('File has not been created in this folder. Please create the file first')
		else:
			print("File has already been created")
			df = pd.read_csv(pathfile)
			numLookup = df[participantName].iloc[0]
			if (newnum < 0):
				newnum = -1 * int(numLookup)
			stringlookup = int(numLookup) + newnum
			df[participantName].iloc[0] = stringlookup
			df.to_csv(pathfile, encoding='utf-8')
			return(stringlookup)
	
def lookupRefSheet(newgame, participantNames):
	game = newgame.lower()
	f = ""; 
	folderpathname = ''
	for root, dirs, files in os.walk("."):
		for dir in dirs:
			if game.casefold() in os.path.basename(dir).casefold():
				folderpathname = dir
				break
	fileName = 'LookUpSheetCounter'	
	pathfile = './' + folderpathname + '/' + fileName + '.csv'
	if folderpathname == '':
		return "Could not find anything"
	else:
		foundFile = False
		for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if fileName.casefold() in os.path.basename(file).casefold():
					print(os.path.basename(fileName).casefold())
					foundFile = True
					break
		if foundFile == False:
			print("here")
			columnentries = [0]
			df = pd.DataFrame(index=columnentries, columns=participantNames)
			df = df.fillna(0)
			df.to_csv(pathfile, encoding='utf-8')
			z = ''
			for item in df.columns.values.tolist():
				z += item + ' \n'
				z += str(df[item].iloc[0]) + '\n'
			return(z)
		else: 
			print("File has already been created")
			df = pd.read_csv(pathfile)
			z = ''
			for item in df.columns.values.tolist():
				z += item + ' \n'
				z += str(df[item].iloc[0]) + '\n'
			return(z)
			
def lookupCommand(argv):
	lm = argv.split(" ")
	print(len(lm))
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = lm[0].lower()
		name = lm[1]
		pokemon = lm[2].upper() 
		f = ""; 
		folderpathname = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		else:
			for root, dirs, files in os.walk("."):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		if f != "":
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			if '.cxi' in f:
				finalString = Lines[5]
			else:
				finalString = Lines[1]
			x = pokemon + ":"
			for line in Lines:
				if "|" + pokemon.casefold() in line.casefold() and pokemon.casefold() + "ite" not in line.casefold() and "|" in line:
					print(line)
					finalString += line
				elif pokemon.casefold() in line.casefold() and pokemon.casefold() and ":" in line:
					print(line)
					finalString += line
				elif pokemon.casefold() in line.casefold() and "->" in line and "by leveling up" not in line.casefold() and "at level" not in line.casefold():
					finalString += line
					index = Lines.index(line)
					newline = ""
					while Lines[index+1] != "\n":
						newline = Lines[index+1]
						finalString += newline
						index += 1
			newfile.close()
			return finalString
		else:
			print("No file was found with that name")
			return("No file was found with that name")


def lookupStarter(argv):
	lm = argv.split(" ")
	print(len(lm))
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = lm[0].lower()
		name = lm[1]
		f = ""; 
		folderpathname = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		else:
			for root, dirs, files in os.walk("."):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		if f != "":
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			if '.cxi' in f:
				finalString = Lines[5]
			else:
				finalString = Lines[1]
			x = pokemon + ":"
			for line in Lines:
				if "starter".casefold() in line.casefold():
					print(line)
					finalString += line
			newfile.close()
			return finalString
		else:
			print("No file was found with that name")
			return("No file was found with that name")
			
def lookupTrainer(argv):
	lm = argv.split(" ")
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = lm[0].lower()
		name = lm[1]
		pokemon = lm[2] 
		f = ""; 
		folderpathname = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		else:
			for root, dirs, files in os.walk("."):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		if f != "":
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			for line in Lines:
				if pokemon in line:
					finalString += line
			newfile.close()
			return finalString
		else:
			print("No file was found with that name")
			return("No file was found with that name")
			
			
def lookupAllTrainer(argv):
	lm = argv.split(" ")
	if (len(lm) != 2):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = lm[0].lower()
		pokemon = lm[1]
		folderpathname = ''
		f = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			finalString = ''
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if os.path.basename(file).casefold() != "rules.txt" and ".txt" in os.path.basename(file).casefold():
						f = file
						pathfile = './' + folderpathname + '/' + f
						finalString += pathfile
						finalString += '\n'
						newfile = open(pathfile, "r", errors='ignore')
						Lines = newfile.readlines()
						x = pokemon + ":"
						for line in Lines:
							if pokemon in line:
								print(line)
								finalString += line + "\n"
						newfile.close()
			help = finalString.split('\n')
			print(len(help))
			return help
		else:
			print("Can't help you")
			return("Can't help you")

def lookupTMS(argv):
	lm = argv.split(" ")
	if (len(lm) != 2):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = lm[0].lower()
		name = name = lm[1]
		f = ""; 
		folderpathname = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		else:
			for root, dirs, files in os.walk("."):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		if f != "":
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			for line in Lines:
				m = re.match("(^TM)", line)
				if m:
					finalString += line
			print(finalString)
			return(finalString)
			
def createRandomizerTrackingFile(argv, name):
	game = argv
	folderpathname = ''
	for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					
					folderpathname = dir
					break
	if folderpathname == '':
		return("I could not find the game you were searching for")
	pathfile = './' + folderpathname + '/' + name + 'logfile.csv'
	foundFile = False
	for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if name.casefold() in os.path.basename(file).casefold():
					foundFile = True
					print("Hi\n\n\n\n\n\n\n\n")
					break
	if foundFile == False:
		columntitles = []
		columntitles.append("# of player resets")
		columntitles.append("# of pokemon deaths")
		columntitles.append("list of pokemon deaths")
		columntitles.append("time taken")
		columntitles.append("# of pokemon caught")
		columntitles.append("list of pokemon caught")
		columnentries = [0]
	
		df = pd.DataFrame(index=columnentries, columns=columntitles)
		df = df.fillna(0)
		df.to_csv(pathfile, encoding='utf-8')

		z = ''
		for item in df.columns.values.tolist():
			z += item + '\n'
			z += str(df[item].iloc[0]) + '\n'
		return(z)
	else: 
		print("File has already been created")
		df = pd.read_csv(pathfile)
		z = ''
		for item in df.columns.values.tolist():
			z += item + '\n'
			z += str(df[item].iloc[0]) + '\n'
		return(z)

		
def updatePokemonDeathCounter(argv, name, pokemon):
	game = argv
	folderpathname = ''
	for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
	pathfile = './' + folderpathname + '/' + name + 'logfile.csv'
	foundFile = False
	for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if name.casefold() in os.path.basename(file).casefold():
					foundFile = True
					print("Hi\n\n\n\n\n\n\n\n")
					break
	if foundFile == False:
		return("You don't have a file to update currently, please make a file")
	else:
		df = pd.read_csv(pathfile)
		z = ''
		numDeath = df['# of pokemon deaths'].iloc[0]
		numDeath = int(numDeath) + 1
		listdeath = df['list of pokemon deaths'].iloc[0]
		if listdeath == 0:
			listdeath = pokemon + ", "
		else:
			listdeath += pokemon + ", "
		df['# of pokemon deaths'].iloc[0] = numDeath
		df['list of pokemon deaths'].iloc[0] = listdeath
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		for item in df.columns.values.tolist():
			z += item + '\n'
			z += str(df[item].iloc[0]) + '\n'
		return(z)
		
def updateResetsCounter(argv, name):
	game = argv
	folderpathname = ''
	for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
	pathfile = './' + folderpathname + '/' + name + 'logfile.csv'
	foundFile = False
	for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if name.casefold() in os.path.basename(file).casefold():
					foundFile = True
					print("Hi\n\n\n\n\n\n\n\n")
					break
	if foundFile == False:
		return("You don't have a file to update currently, please make a file")
	else:
		df = pd.read_csv(pathfile)
		z = ''
		numDeath = df['# of player resets'].iloc[0]
		numDeath = int(numDeath) + 1
		df['# of player resets'].iloc[0] = numDeath
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		for item in df.columns.values.tolist():
			z += item + '\n'
			z += str(df[item].iloc[0]) + '\n'
		return(z)
	

def updateTimeCounter(argv, name, pokemon):
	game = argv
	folderpathname = ''
	for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
	pathfile = './' + folderpathname + '/' + name + 'logfile.csv'
	foundFile = False
	for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if name.casefold() in os.path.basename(file).casefold():
					foundFile = True
					print("Hi\n\n\n\n\n\n\n\n")
					break
	if foundFile == False:
		return("You don't have a file to update currently, please make a file")
	else:
		df = pd.read_csv(pathfile)
		z = ''
		timetaken = df['time taken'].iloc[0]
		timetaken = int(timetaken) + pokemon
		df['time taken'].iloc[0] = timetaken
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		for item in df.columns.values.tolist():
			z += item + '\n'
			z += str(df[item].iloc[0]) + '\n'
		return(z)	
		
def updatePokemonCaughtCounter(argv, name, pokemon):
	game = argv
	folderpathname = ''
	for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
	pathfile = './' + folderpathname + '/' + name + 'logfile.csv'
	foundFile = False
	for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if name.casefold() in os.path.basename(file).casefold():
					foundFile = True
					print("Hi\n\n\n\n\n\n\n\n")
					break
	if foundFile == False:
		return("You don't have a file to update currently, please make a file")
	else:
		df = pd.read_csv(pathfile)
		z = ''
		numDeath = df['# of pokemon caught'].iloc[0]
		numDeath = int(numDeath) + 1
		listdeath = df['list of pokemon caught'].iloc[0]
		if listdeath == 0:
			listdeath = pokemon + ", "
		else:
			listdeath += pokemon + ", "
		df['# of pokemon caught'].iloc[0] = numDeath
		df['list of pokemon caught'].iloc[0] = listdeath
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		for item in df.columns.values.tolist():
			z += item + '\n'
			z += str(df[item].iloc[0]) + '\n'
		return(z)
		
#For formula, make new variable that logs all the pokemon death after a reset. 
#Add gym as a requirement
		
def calculateRandomizerSuccessFormulaForSelf(argv, name):
	#closer to 0
	game = argv
	folderpathname = ''
	for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
	pathfile = './' + folderpathname + '/' + name + 'logfile.csv'
	foundFile = False
	for root, dirs, files in os.walk(folderpathname):
			for file in files:
				if name.casefold() in os.path.basename(file).casefold():
					foundFile = True
					print("Hi\n\n\n\n\n\n\n\n")
					break
	if foundFile == False:
		return("You don't have a file to update currently, please make a file")
	else:
		df = pd.read_csv(pathfile)
		z = ''
		formula = 0
		numReset = df['# of player resets'].iloc[0]
		numDeath = df['# of pokemon deaths'].iloc[0]
		numCaught = df['# of pokemon caught'].iloc[0]
		timeTaken = df['time taken'].iloc[0]
		if numReset == 0:
			formula = (3*int(numDeath)-2*int(numCaught)) * (int(timeTaken)/2)
		elif numReset == 1:
			formula = (int(numReset) + .5)* (3*int(numDeath)-2*int(numCaught)) * (int(timeTaken)/2)
		else:
			formula = int(numReset) * (3*int(numDeath)-2*int(numCaught)) * (int(timeTaken)/2)
		z = name + '\'s current nuzlocke value for the ' + folderpathname + ' is: ' + str(formula)
		return(z)
			
			
def rankAllRandomizerFormula(argv):
	game = argv
	folderpathname = ''
	for root, dirs, files in os.walk("."):
		for dir in dirs:
			if game.casefold() in os.path.basename(dir).casefold():
				folderpathname = dir
				break
	z = {}
	for root, dirs, files in os.walk(folderpathname):
		for file in files:
			if '.csv' in os.path.basename(file).casefold() and 'lookupsheetcounter.csv' != os.path.basename(file).casefold():
				pathfile = file
				print(file)
				df = pd.read_csv('./' + folderpathname + '/' + pathfile)
				numReset = df['# of player resets'].iloc[0]
				numDeath = df['# of pokemon deaths'].iloc[0]
				numCaught = df['# of pokemon caught'].iloc[0]
				timeTaken = df['time taken'].iloc[0]
				if numReset == 0:
					#fix this asap
					formula = 0
				elif numReset == 1:
					formula = (int(numReset) + .5)* (int(numDeath)-int(numCaught)) * (int(timeTaken)/2)
				else:
					formula = int(numReset) * (int(numDeath)-int(numCaught)) * (int(timeTaken)/2)
				name = os.path.basename(pathfile).split('logfile.csv')[0]
				newdict = {name: formula}
				z.update(newdict)
	
	z = sorted(z.items(), key=lambda x: x[1])
	s = ''
	for key, value in z:
		s += key + "'s value is: " + str(value)
		s += '\n'
	print(s)
	return(s)

def lookupRoute(argv):
	lm = argv.split(" ", 2)
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = lm[0].lower()
		name = name = lm[1]
		pokemon = lm[2]
		f = ""; 
		folderpathname = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		else:
			for root, dirs, files in os.walk("."):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		if f != "":
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			for line in Lines:
				if pokemon in line:
					finalString += line
					finalString += 'jeezypeezy'
			newfile.close()
			help = finalString.split('jeezypeezy')
			print(len(help))
			return help
		else:
			print("No file was found with that name")
			return("No file was found with that name")

def lookupAllPokemon(argv):
	lm = argv.split(" ")
	if (len(lm) != 2):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = lm[0].lower()
		pokemon = lm[1].upper()
		folderpathname = ''
		f = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			finalString = ''
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if os.path.basename(file).casefold() != "rules.txt" and ".txt" in os.path.basename(file).casefold():
						f = file
						pathfile = './' + folderpathname + '/' + f
						finalString += pathfile
						finalString += '\n'
						newfile = open(pathfile, "r", errors='ignore')
						Lines = newfile.readlines()
						finalString += Lines[1]
						x = pokemon + ":"
						for line in Lines:
							if pokemon in line and "|" in line:
								#print(line)
								finalString += line
							if pokemon in line and ":" in line:
								#print(line)
								finalString += line
						newfile.close()
						finalString += 'jeezypeezy'
			help = finalString.split('jeezypeezy')
			print(len(help))
			return help
		else:
			print("Can't help you")
			return("Can't help you")


#-----------------------------------------------------			
def lookupAllPokemonBase(argv):
	if (len(sys.argv) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = sys.argv[1].lower()
		pokemon = sys.argv[2].upper()
		folderpathname = ''
		f = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					f = file
					pathfile = './' + folderpathname + '/' + f
					f += '\n'
					newfile = open(pathfile, "r", errors='ignore')
					Lines = newfile.readlines()
					finalString = Lines[1]
					x = pokemon + ":"
					for line in Lines:
						if pokemon in line and "|" in line:
							print(line)
							finalString += line
						if pokemon in line and ":" in line:
							print(line)
							finalString += line
				newfile.close()
			return finalString
		else:
			print("Can't help you")
			return("Can't help you")
		
			
def lookupTrainerBase(argv):
	if (len(sys.argv) != 4):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = sys.argv[1].lower()
		name = sys.argv[2]
		pokemon = sys.argv[3]
		f = ""; 
		folderpathname = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		else:
			for root, dirs, files in os.walk("."):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		if f != "":
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			for line in Lines:
				if pokemon in line:
					print(line)
					finalString += line
			newfile.close()
			return finalString
		else:
			print("No file was found with that name")
			return("No file was found with that name")
			
			
def lookupCommandBase(argv):
	if (len(sys.argv) != 4):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	else:
		game = sys.argv[1].lower()
		name = sys.argv[2]
		pokemon = sys.argv[3].upper() 
		f = "" 
		folderpathname = ''
		for root, dirs, files in os.walk("."):
			for dir in dirs:
				if game.casefold() in os.path.basename(dir).casefold():
					folderpathname = dir
					print(dir)
					break
		if folderpathname != '':
			for root, dirs, files in os.walk(folderpathname):
				for file in files:
					print(file)
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		else:
			for root, dirs, files in os.walk("."):
				for file in files:
					if name.casefold() in os.path.basename(file).casefold():
						f = file
						break
		print(f)
		if f != "":
			finalString = ''
			pathfile = './' + folderpathname + '/' + f
			newfile = open(pathfile, "r", errors='ignore')
			Lines = newfile.readlines()
			finalString = Lines[1]
			x = pokemon + ":"
			for line in Lines:
				if pokemon in line and "|" in line:
					print(line)
					finalString += line
				if pokemon in line and ":" in line:
					print(line)
					finalString += ' '
					finalString += line
			newfile.close()
			return finalString
		else:
			print("No file was found with that name")
			return("No file was found with that name")

#---------------------------------------------------------------
