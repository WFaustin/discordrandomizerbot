import sys
import os
import re
import pandas as pd

def main(argv):
	rankAllRandomizerFormula(argv[0])
	
def checkIfFileIsAllowed(arguments):
	#arguments should be split
	#placeholder should be ###
	filename = "."
	for a in range(0, len(arguments)):
		if a == len(arguments) - 1:
			for root, dirs, files in os.walk(filename):
				for file in files:
					if arguments[a].casefold() in os.path.basename(file).casefold():
						filename += "/" + file
						break
		else:
			for root, dirs, files in os.walk(filename):
				for dir in dirs:
					if arguments[a].casefold() in os.path.basename(dir).casefold():
						filename += "/" + dir
						break
		#if a != "###":
		#	filename += "/" + a
	print(filename)
	if os.path.isfile(filename) == True: 
		return([True, filename])
	else:
		return([False])

#Use for all the "ALL" functions
def checkifDirIsAllowed(arguments):
	dirname = "./"
	for a in range(0, len(arguments)):
		for root, dirs, files in os.walk(dirname):
			for dir in dirs:
				if arguments[a].casefold() in os.path.basename(dir).casefold():
					dirname += dir + "/"
					break
	print(dirname)
	if os.path.isdir(dirname) == True:
		return([True, dirname])
	else:
		return([False])

#Outputs rules
def outputRules(text):
	check = checkIfFileIsAllowed([text[0], "rules"])
	if check[0] == True:
		print(check[1])
		newfile = open(check[1], 'r', errors='ignore')
		Lines = newfile.readlines()
		return(Lines)
	else:
		return(["Couldn't find any RULES for this game"])

#Adds punishment suggestions from users
def addPunishmentSuggestions(text):
	s = ['PunishmentSuggestions.txt']
	check = checkIfFileIsAllowed(s)
	if check[0] == True:
		newfile = open(check[1], "r", errors='ignore')
		newfile.write(text + '\n')
		print("Updated")
		newfile.close()
		return viewPunishmentSuggestions()
	else:
		return("Couldn't find the Punishment files, check your directory")
	return

#shows punishment suggestions
def viewPunishmentSuggestions():
	text = ['PunishmentSuggestions.txt']
	check = checkIfFileIsAllowed(text)
	if check[0] == True:
		newfile = open(check[1], 'a', errors='ignore')
		Lines = newfile.readlines()
		return(Lines)
	else:
		return("Couldn't find the Punishment files, check your directory")
	return

#May not need this as this probably should be chosen by the bot. 
def getPunishmentSuggestionsWheelLink():
	return

#Looking up a person's reference sheet based on their scoring
def lookupRefSheet(newgame, participantNames):
	return

#Lookup a person's nuzlocke textfile for any sort of pokemon
def lookupCommand(argv):
	lm = argv.split(" ")
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	check = checkIfFileIsAllowed([lm[0].lower(), lm[1]])
	if check[0] == True:
		newfile = open(check[1], "r", errors='ignore')
		Lines = newfile.readlines()
		pokemon = lm[2]
		if '.cxi' in check[1]:
			finalString = Lines[5]
		else:
			finalString = Lines[1]
		x = pokemon + ":"
		for line in Lines:
			if "|" + pokemon.casefold() in line.casefold() and pokemon.casefold() + "ite" not in line.casefold() and "|" in line:
				print(line)
				finalString += line
			elif pokemon.casefold() in line.casefold() and pokemon.casefold() and ":" in line:
				print(line)
				finalString += line
			elif pokemon.casefold() in line.casefold() and "->" in line and "by leveling up" not in line.casefold() and "at level" not in line.casefold():
				finalString += line
				index = Lines.index(line)
				newline = ""
				while Lines[index+1] != "\n":
					newline = Lines[index+1]
					finalString += newline
					index += 1
		newfile.close()
		return finalString
	else:
		return("No file was found with that name")

#Lookup a person's nuzlocke textfile for their specific starter
def lookupStarter(argv):
	lm = argv.split(" ")
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	check = checkIfFileIsAllowed([lm[0].lower(), lm[1]])
	if check[0] == True:
		pokemon = lm[2]
		newfile = open(check[1], "r", errors='ignore')
		Lines = newfile.readlines()
		if '.cxi' in f:
			finalString = Lines[5]
		else:
			finalString = Lines[1]
		x = pokemon + ":"
		for line in Lines:
			if "starter".casefold() in line.casefold():
				finalString += line
		newfile.close()
		return finalString
	else:
		return "Can't find it"

#Lookup a person's nuzlocke textfile for any trainer
def lookupTrainer(argv):
	lm = argv.split(" ")
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	check = checkIfFileIsAllowed([lm[0].lower(), lm[1]])
	if check[0] == True:
		finalString = ""
		newfile = open(check[1], "r", errors='ignore')
		Lines = newfile.readlines()
		trainerName = lm[2]
		for line in Lines:
			if trainerName in line:
				finalString += line
		newfile.close()
		if finalString == "":
			return "Could not find trainer name " + trainerName
		else:
			return finalString
	else:
		print("Error Finding File")
		return("Error Finding File")

def lookupRoute(argv):
	lm = argv.split(" ", 2)
	if (len(lm) != 3):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	check = checkIfFileIsAllowed([lm[0].lower(), lm[1]])
	if check[0] == True:
		finalString = ""
		newfile = open(check[1], "r", errors='ignore')
		Lines = newfile.readlines()
		routeName = lm[2]
		for line in Lines:
			if routeName in line:
				print(routeName)
				finalString += line
				finalString += 'jeezypeezy'
		newfile.close()
		help = finalString.split('jeezypeezy')
		if help[0] == "":
			return "Could not find route: " + routeName
		else:
			return help
	else:
		print("Error Finding File")
		return("Error Finding File")

#Lookup 
def lookupAllTrainer(argv):
	lm = argv.split(" ")
	if (len(lm) != 2):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	check = checkifDirIsAllowed([lm[0].lower()])
	if check[0] == True:
		allLogFiles = []
		trainerName = lm[1]
		finalString = ''
		for root, dirs, files in os.walk(check[1]):
			for file in files:
				if ".log" in os.path.basename(file).casefold():
					pathfile = './' + check[1] + file
					finalString += pathfile
					finalString += '\n'
					newfile = open(pathfile, "r", errors='ignore')
					Lines = newfile.readlines()
					x = trainerName + ":"
					for line in Lines:
						if trainerName in line:
							print(line)
							finalString += line + "\n"
					newfile.close()
		help = finalString.split('\n')
		print(help)
		if len(help) == 0:
			return "Found File But Searched Name Isn't In The Results"
		return help
	else:
		return "Couldn't find any files to search for"

def lookupTMS(argv):
	lm = argv.split(" ")
	if (len(lm) != 2):
		print("Can't look up the pokemon you want due to incorrect format")
		return("Can't look up the pokemon you want due to incorrect format")
	check = checkIfFileIsAllowed([lm[0].lower(), lm[1]])
	if check[0] == True:
		finalString = ""
		newfile = open(check[1], "r", errors='ignore')
		Lines = newfile.readlines()
		for line in Lines:
			m = re.match("(^TM)", line)
			if m:
				finalString += line
		newfile.close()
		return finalString
	else:
		print("Error Finding File")
		return("Error Finding File")


#Create the randomizer tracking file for each person
def createRandomizerTrackingFile(game, name):
	name += "logfile.csv"
	df = ''
	z = ''
	check = checkIfFileIsAllowed([game, name])
	if check[0] == True:
		s = ""
		print("File has already been created")
		df = pd.read_csv(check[1])
	else:
		nextcheck = checkifDirIsAllowed([game])
		columntitles = ["score", "# of player resets", "# of lookups" ,"time taken (minutes)", "# of pokemon caught", "# of pokemon deaths","list of pokemon deaths", "list of pokemon caught"]
		columnentries = [0]
		df = pd.DataFrame(index=columnentries, columns=columntitles)
		df = df.fillna(0)
		df.to_csv(nextcheck[1] + name, encoding='utf-8')
	for item in df.columns.values.tolist():
		z += item + '\n'
		z += str(df[item].iloc[0]) + '\n'
	return z


#Updating a person's reference sheet for scoring
#Probably should be refactored
#Name change from updatePersonRefSheetNum to updateNumOfLookups
def updatePersonRefSheetNum(newgame, participantName, newnum=1):
	name = participantName + ".csv"
	check = checkIfFileIsAllowed([newgame, name])
	if check[0] == True:
		s = ""
		print("File has already been created")
		df = pd.read_csv(pathfile)
		numLookups = int(df['# of lookups'].iloc[0]) + 1
		df['# of lookups'].iloc[0] = numLookups
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		return(calculateRandomizerSuccessFormulaForSelf(game, name))
	else:
		return("This person does not have a file affiliated with this game")

def updatePokemonDeathCounter(game, name, pokemon):
	check = checkIfFileIsAllowed([game.lower(), name])
	if check[0] == True:
		s = ""
		pathfile = check[1]
		df = pd.read_csv(pathfile)
		numDeath = df['# of pokemon deaths'].iloc[0]
		numDeath = int(numDeath) + 1
		listdeath = df['list of pokemon deaths'].iloc[0]
		numLookups = int(df['# of lookups'].iloc[0]) + 1
		if listdeath == 0:
			listdeath = pokemon
		else:
			listdeath +=  ", " + pokemon
		df['# of pokemon deaths'].iloc[0] = numDeath
		df['list of pokemon deaths'].iloc[0] = listdeath
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		return(calculateRandomizerSuccessFormulaForSelf(game, name))
	else:
		return "Couldn't find file to update, please create a file using !createSheet (name of game)"

def updateResetsCounter(game, name):
	check = checkIfFileIsAllowed([game.lower(), name])
	if check[0] == True:
		s = ""
		pathfile = check[1]
		df = pd.read_csv(pathfile)
		numDeath = df['# of player resets'].iloc[0]
		numDeath = int(numDeath) + 1
		df['# of player resets'].iloc[0] = numDeath
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		return(calculateRandomizerSuccessFormulaForSelf(game, name))
	else:
		return "Couldn't find file to update, please create a file using !createSheet (name of game)"

def updateTimeCounter(game, name, pokemon):
	check = checkIfFileIsAllowed([game.lower(), name])
	if check[0] == True:
		s = ""
		pathfile = check[1]
		df = pd.read_csv(pathfile)
		timetaken = df['time taken (minutes)'].iloc[0]
		timetaken = int(timetaken) + pokemon
		df['time taken (minutes)'].iloc[0] = timetaken
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		return(calculateRandomizerSuccessFormulaForSelf(game, name))
	else:
		return "Couldn't find file to update, please create a file using !createSheet (name of game)"

def updatePokemonCaughtCounter(game, name, pokemon):
	check = checkIfFileIsAllowed([game.lower(), name])
	if check[0] == True:
		s = ""
		pathfile = check[1]
		df = pd.read_csv(pathfile)
		numDeath = df['# of pokemon caught'].iloc[0]
		numDeath = int(numDeath) + 1
		listdeath = df['list of pokemon caught'].iloc[0]
		if listdeath == 0:
			listdeath = pokemon + ", "
		else:
			listdeath += pokemon + ", "
		df['# of pokemon caught'].iloc[0] = numDeath
		df['list of pokemon caught'].iloc[0] = listdeath
		for column in df.columns.values:
			if "Unnamed" in column:
				del df[column]
		df.to_csv(pathfile, encoding='utf-8')
		return(calculateRandomizerSuccessFormulaForSelf(game, name))
	else:
		return "Couldn't find file to update, please create a file using !createSheet (name of game)"

def calculateRandomizerSuccessFormulaForSelf(game, name):
	check = checkIfFileIsAllowed([game.lower(), name])
	if check[0] == True:
		s = ""
		pathfile = check[1]
		df = pd.read_csv(pathfile)
		formula = df['score'].iloc[0]
		numReset = df['# of player resets'].iloc[0]
		numDeath = df['# of pokemon deaths'].iloc[0]
		numCaught = df['# of pokemon caught'].iloc[0]
		timeTaken = df['time taken (minutes)'].iloc[0]
		if numCaught == 0:
			numCaught = 1
		if numReset == 0:
			numReset = .5
		formula = numReset*((timeTaken/(4*numCaught)) + (1.5*((3*numDeath)/(2*numCaught))))
		df['score'].iloc[0] = formula
		df.to_csv(pathfile, encoding='utf-8')
		return(createRandomizerTrackingFile(game, name))
	else:
		return "Couldn't find file to update, please create a file using !createSheet (name of game)"

def rankAllRandomizerFormula(game):
	check = checkifDirIsAllowed([game.lower()])
	if check[0] == True:
		s = ""
		pathfile = check[1]
		scoreRankings = {}
		points = []

		for root, dirs, files in os.walk(check[1]):
			for file in files:
				if "logfile.csv" in os.path.basename(file).casefold():
					name = os.path.basename(file).casefold().split("logfile.csv")
					df = pd.read_csv(check[1] + "/" + file)
					print(df['score'].iloc[0])
					scoreRankings.update({name[0]: df['score'].iloc[0]})
					points.append(df['score'].iloc[0])
		print(scoreRankings)
		points.sort()
		checked = []
		outputScore = []
		for i in points:
			for key, value in scoreRankings.items():
				if i == value and key not in checked:
					checked.append(key)
					outputScore.append(str(key) + " with a score of " + str(value))
					break
		return outputScore
	else:
		return "Couldn't find the specified folder"



if __name__ == "__main__":
   main(sys.argv[1:])		