#discordbot.py

import os
import random
import randombot as rb
import serverbot as sb
import time

import discord
from discord.ext import commands
from dotenv import load_dotenv



load_dotenv()

TOKEN = os.getenv('DISCORD_TOKEN')

GUILD = os.getenv('DISCORD_GUILD')

firstitToken = TOKEN.split('{')
printableToken = firstitToken[1].split('}')
token = printableToken[0]

timed = True
start_timer = time.perf_counter()
stop_timer = 0

rl = False
LookupMarker = False
maxLookups = 13

tester = False

bot = commands.Bot(command_prefix='!')
channeltestnum = 717213124930830406
randomizerComp = 718859717367169044
randomizerPublic = 718859890625478709
role_id = 718858488771510343
my_role = 718862987430985758
emoji_cool='\N{THUMBS UP SIGN}'
emoji_uncool='\N{THUMBS DOWN SIGN}'

if tester == True:
	randomizerComp = 717213124930830406

#client = discord.Client()

#@client.event
#async def on_ready():
#	for guild in client.guilds:
#		if guild.name == GUILD:
#			break
	
#	print(f'{client.user} has been connected to discord!\n' f'{guild.name}(id: {guild.id})\n')

#	channel = client.get_channel(717213124930830406)
#	await channel.send('Hello, I am the randomizer bot. I am alive and ready to help.')
	#692097522335875162
	#await guild.members[0].create_dm()
	#await guild.members[0].dm_channel.send(
	#	f'I am alive and ready to help.'
	#)
	#members = '\n - '.join([member.name for member in guild.members])
	#print(f'Guild Members:\n - {members}')
	#channels = '\n - '.join([channel.name for channel in guild.channels])
	#print(f'Guild Channels:\n - {channels}')
	#print(f'Guild Channels:\n - {guild.channels}')
	#
	#
	#
	#
	#
	#
	#
	#
	#

	
	

@bot.event
async def on_ready():
	print(f'{bot.user.name} has been connected to discord!\n')
	channel = bot.get_channel(randomizerComp)
	await channel.send('Manny is up and running..... For now at least.')
	for guild in bot.guilds:
		if guild.name == GUILD:
			break
	
	emoji = guild.emojis
	members = '\n - '.join([member.name for member in guild.members])
	print(f'Guild Members:\n - {members}')
	channels = '\n - '.join([channel.name for channel in guild.channels])
	print(f'Guild Channels:\n - {channels}')
	print(f'Guild Channels:\n - {guild.channels}')
	print(guild.roles)
	print(emoji)
	
@bot.command(name='rules', help='Gives rules for the game specified')
async def rulesOutput(ctx, *, arg: str):
	await ctx.message.add_reaction(emoji_cool)
	s = rb.outputRules([arg])
	await ctx.send(f'Here\'s the rules for the specified game. ')	
	for help in s:
		await ctx.send('```' + help + '```')
	
@bot.command(name='spinWheel', help='Spin Wheel manually instead of using the link')
async def spinWheel(ctx):
	s = rb.viewPunishmentSuggestions()
	largeText = ''
	for i in s:
		largeText += i
	b = largeText.split("\"\n")
	response = random.choice(b)
	await ctx.send(f'The random punishment {ctx.message.author.mention} has received is: ' + str(response))	

@bot.command(name='addPunishmentSuggestions', help='Add new punishment suggestions to the punishment suggestion sheet')
async def add_Punishment_Suggestions(ctx, *, arg: str):
	channel = bot.get_channel(randomizerComp)
	channel2 = bot.get_channel(randomizerPublic)
	textbox = ''
	x = arg.split(" ")
	for i in x:
		textbox += i + " "
	finaltextbox = "\"" + textbox + "\""
	await ctx.message.add_reaction(emoji_cool)
	s = rb.addPunishmentSuggestions(finaltextbox)
	await ctx.send('Done')
	#await channel.send("Outputting the punishment suggestion sheet now")
	#await channel2.send("Outputting the punishment suggestion sheet now")
	#for help in s:
	#	await channel.send('```' + help + '```')
	#	await channel2.send('```' + help + '```')
		
		
@bot.command(name='viewPunishmentSuggestions', help='View punishment suggestions')
async def view_Punishment_Suggestions(ctx):
	channel = bot.get_channel(randomizerComp)
	channel2 = bot.get_channel(randomizerPublic)
	await ctx.message.add_reaction(emoji_cool)
	s = rb.viewPunishmentSuggestions()
	await channel.send("Outputting the punishment suggestion sheet now")
	await channel2.send("Outputting the punishment suggestion sheet now")
	for help in s:
		await channel.send('```' + help + '```')
		await channel2.send('```' + help + '```')
	

@bot.command(name='pokemon', help='Gives sheet of a person and returns the pokemon\'s info and levelset')
async def lookup_Sheet_For_Person(ctx, *, arg: str):
	global maxLookups
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	#print("Yes, I've got the message")
	s = rb.lookupCommand(arg)
	if len(s) >= 2000:
		st = s.split("\n")
		for help in st:
		#	print("hi")
			if help != '':
				await channel.send('```' + help + '```')
	else:
		await channel.send('```' + s + '```')
	global LookupMarker
	if LookupMarker == True:
		print("Hello")
		t = arg.split(" ")
		p = rb.updatePersonRefSheetNum(t[0], ctx.message.author.name, 1)
		await ctx.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		await channel.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		if (p > maxLookups):
			await channel.send(f'{ctx.message.author.mention} has went over the limit! It\'s time for a punishment! SPIN THE WHEEL!')


@bot.command(name='starter', help='Gives sheet of a person and returns the starters')
async def lookup_Sheet_For_Starter(ctx, *, arg: str):
	global maxLookups
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	#print("Yes, I've got the message")
	s = rb.lookupCommand(arg)
	if len(s) >= 2000:
		st = s.split("\n")
		for help in st:
		#	print("hi")
			if help != '':
				await channel.send('```' + help + '```')
	else:
		await channel.send('```' + s + '```')
	global LookupMarker
	if LookupMarker == True:
		print("Hello")
		t = arg.split(" ")
		p = rb.updatePersonRefSheetNum(t[0], ctx.message.author.name, 1)
		await ctx.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		await channel.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		if (p > maxLookups):
			await channel.send(f'{ctx.message.author.mention} has went over the limit! It\'s time for a punishment! SPIN THE WHEEL!')
	
@bot.command(name='trainer', help='Looks up the sheet of a person and returns the trainer\'s info ')
async def lookup_Sheet_For_Trainer(ctx, *, arg: str):
	global maxLookups
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	#print("Yes, I've got the message")
	s = rb.lookupTrainer(arg)
	await channel.send('```' + s + '```')
	global LookupMarker
	if LookupMarker == True:
		print("Hello")
		t = arg.split(" ")
		p = rb.updatePersonRefSheetNum(t[0], ctx.message.author.name, 1)
		await ctx.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		await channel.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		if (p > maxLookups):
			await channel.send(f'{ctx.message.author.mention} has went over the limit! It\'s time for a punishment! SPIN THE WHEEL!')
	
@bot.command(name='pokemonAll', help='Gives all the sheets and returns the specific pokemon\'s info and levelset')
async def lookup_Sheet_For_PokemonAll(ctx, *, arg: str):
	global maxLookups
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	#print("Yes, I've got the message")
	s = rb.lookupAllPokemon(arg)
	#print(len(s))
	for help in s:
	#	print("hi")
		if help != '':
			await channel.send('```' + help + '```')
	global LookupMarker
	if LookupMarker == True:
		print("Hello")
		t = arg.split(" ")
		p = rb.updatePersonRefSheetNum(t[0], ctx.message.author.name, 2)
		await ctx.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		await channel.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		if (p > maxLookups):
			await channel.send(f'{ctx.message.author.mention} has went over the limit! It\'s time for a punishment! SPIN THE WHEEL!')
			
@bot.command(name='trainerAll', help='Gives all the sheets and returns the trainer\'s info ')
async def lookup_Sheet_For_TrainerAll(ctx, *, arg: str):
	global maxLookups
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	#print("Yes, I've got the message")
	s = rb.lookupAllTrainer(arg)
	for help in s:
	#	print("hi")
		if help != '':
			await channel.send('```' + help + '```')
	global LookupMarker
	if LookupMarker == True:
		print("Hello")
		t = arg.split(" ")
		p = rb.updatePersonRefSheetNum(t[0], ctx.message.author.name, 2)
		await ctx.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		await channel.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		if (p > maxLookups):
			await channel.send(f'{ctx.message.author.mention} has went over the limit! It\'s time for a punishment! SPIN THE WHEEL!')
			
			
@bot.command(name='tms', help='Gives sheet of a person and returns all the TMs')
async def lookup_Sheet_For_TMS(ctx, *, arg: str):
	global maxLookups
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	#print("Yes, I've got the message")
	s = rb.lookupTMS(arg)
	#print(len(s))
	await channel.send('```' + s + '```')
	global LookupMarker
	if LookupMarker == True:
		print("Hello")
		t = arg.split(" ")
		p = rb.updatePersonRefSheetNum(t[0], ctx.message.author.name, 2)
		await ctx.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		await channel.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		if (p > maxLookups):
			await channel.send(f'{ctx.message.author.mention} has went over the limit! It\'s time for a punishment! SPIN THE WHEEL!')
	
@bot.command(name='route', help='Gives sheet of a person and returns the pokemon in a specific route')
async def lookup_Sheet_For_Route(ctx, *, arg: str):
	global maxLookups
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	print(len(arg))
	#print("Yes, I've got the message")
	s = rb.lookupRoute(arg)
	#print(len(s))
	for help in s:
	#	print("hi")
		if help != '':
			await channel.send('```' + help + '```')
	global LookupMarker
	if LookupMarker == True:
		print("Hello")
		t = arg.split(" ")
		p = rb.updatePersonRefSheetNum(t[0], ctx.message.author.name, 1)
		await ctx.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		await channel.send(f'Number of times {ctx.message.author.mention} looked up info from a sheet: ' + str(p))
		if (p > maxLookups):
			await channel.send(f'{ctx.message.author.mention} has went over the limit! It\'s time for a punishment! SPIN THE WHEEL!')
			
			
@bot.command(name='createSheet', help='creates a sheet that keeps track for a randomizer game')
async def createSheet_For_Game(ctx, *, arg: str):
	l = False
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == role_id:
			l = True
			break
	if l == True:
		channel = bot.get_channel(randomizerComp)
		await ctx.message.add_reaction(emoji_cool)
		arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
		await channel.send(arguments)
	#print("Yes, I've got the message")
		teen = arg.split(" ", 1)
		s = rb.createRandomizerTrackingFile(teen[0], ctx.message.author.name)
	#print(len(s))
		t = s.split("\n")
		for help in t:
	#	print("hi")
			if help != '':
				await channel.send('```' + help + '```')
	else:
		channel = bot.get_channel(randomizerComp)
		await channel.send(f'Can\'t seem to do this, you don\'t have the proper role {ctx.message.author.mention}')	

@bot.command(name='updateDeath', help='updates the sheet that keeps track for a randomizer game')
async def update_List_of_Dead_Pokemon(ctx, *, arg: str):
	l = False
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == role_id:
			l = True
			break
	if l == True:
		channel = bot.get_channel(randomizerComp)
		await ctx.message.add_reaction(emoji_cool)
		arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
		await channel.send(arguments)
	#print("Yes, I've got the message")
		teen = arg.split(" ", 1)
		s = rb.updatePokemonDeathCounter(teen[0], ctx.message.author.name, teen[1])
	#print(len(s))
		t = s.split("\n")
		for help in t:
	#	print("hi")
			if help != '':
				await channel.send('```' + help + '```')
	else:
		channel = bot.get_channel(randomizerComp)
		await channel.send(f'Can\'t seem to do this, you don\'t have the proper role {ctx.message.author.mention}')


@bot.command(name='updateCaught', help='updates the sheet that keeps track for a randomizer game')
async def update_List_of_Caught_Pokemon(ctx, *, arg: str):
	l = False
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == role_id:
			l = True
			break
	if l == True:
		channel = bot.get_channel(randomizerComp)
		await ctx.message.add_reaction(emoji_cool)
		arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
		await channel.send(arguments)
	#print("Yes, I've got the message")
		teen = arg.split(" ", 1)
		s = rb.updatePokemonCaughtCounter(teen[0], ctx.message.author.name, teen[1])
	#print(len(s))
		t = s.split("\n")
		for help in t:
	#	print("hi")
			if help != '':
				await channel.send('```' + help + '```')
	else:
		channel = bot.get_channel(randomizerComp)
		await channel.send(f'Can\'t seem to do this, you don\'t have the proper role {ctx.message.author.mention}')
		
		
@bot.command(name='updateReset', help='updates the sheet that keeps track for a randomizer game')
async def update_num_of_Resets(ctx, *, arg: str):
	l = False
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == role_id:
			l = True
			break
	if l == True:
		channel = bot.get_channel(randomizerComp)
		await ctx.message.add_reaction(emoji_cool)
		arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
		await channel.send(arguments)
	#print("Yes, I've got the message")
		teen = arg.split(" ", 1)
		s = rb.updateResetsCounter(teen[0], ctx.message.author.name)
	#print(len(s))
		p = rb.updatePersonRefSheetNum(teen[0], ctx.message.author.name, -1)
		t = s.split("\n")
		for help in t:
	#	print("hi")
			if help != '':
				await channel.send('```' + help + '```')
	else:
		channel = bot.get_channel(randomizerComp)
		await channel.send(f'Can\'t seem to do this, you don\'t have the proper role {ctx.message.author.mention}')
		
@bot.command(name='updateTime', help='updates the sheet that keeps track for a randomizer game')
async def update_Time_Taken(ctx, *, arg: str):
	l = False
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == role_id:
			l = True
			break
	if l == True:
		channel = bot.get_channel(randomizerComp)
		await ctx.message.add_reaction(emoji_cool)
		arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
		await channel.send(arguments)
		s = ''
		teen = arg.split(" ")
		time = teen[1].split(':')
		if len(time) == 1 and time[0].isdigit():
			s = rb.updateTimeCounter(teen[0], ctx.message.author.name, int(time[0]))
		elif len(time) == 2 and time[0].isdigit() and time[1].isdigit():
			total = int(time[0]) * 60 + int(time[1])
			s = rb.updateTimeCounter(teen[0], ctx.message.author.name, total)
		else:
			await ctx.message.add_reaction(emoji_uncool)
			await channel.send('Can\'t do this, time wasn\'t given properly. Will still output sheet.')
			s = rb.createRandomizerTrackingFile(teen[0], ctx.message.author.name)
		
		t = s.split("\n")
		for help in t:
			if help != '':
				await channel.send('```' + help + '```')
	else:
		channel = bot.get_channel(randomizerComp)
		await channel.send(f'Can\'t seem to do this, you don\'t have the proper role {ctx.message.author.mention}')


@bot.command(name='public', help='shows the public every sheet for a particular game')
async def publicCreate_or_Update(ctx, arg: str):
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	for myguild in bot.guilds:
		if myguild.name == GUILD:
			break
	#print(myguild)
	members = myguild.members
	validmembers = []
	for member in members:
		for role in member.roles:
			if role.id == role_id:
				validmembers.append(member)
	for mem in validmembers:
		s = rb.createRandomizerTrackingFile(arg, mem.name)
		t = s.split("\n")
		await channel.send(f'{mem.mention}\'s File')
		for help in t:
	#	print("hi")
			if help != '':
				channel = bot.get_channel(randomizerComp)
				await channel.send('```' + help + '```')
				channel = bot.get_channel(randomizerPublic)
				await channel.send('```' + help + '```')
			elif (t.index(help) != len(t) - 1 or t.index(help) != 0) and len(t) == 1:
				channel = bot.get_channel(randomizerComp)
				await channel.send(f'I can\'t seem to find the file of {mem.mention}')		


@bot.command(name='formula', help='Calculates the person\'s randomizer success value')
async def update_Calc_Formula(ctx, *, arg: str):
	l = False
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == role_id:
			l = True
			break
	if l == True:
		channel = bot.get_channel(randomizerComp)
		await ctx.message.add_reaction(emoji_cool)
		arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
		await channel.send(arguments)		
		teen = arg.split(" ", 1)
		s = rb.calculateRandomizerSuccessFormulaForSelf(teen[0], ctx.message.author.name)
	#print(len(s))
		await channel.send('```' + s + '```')
	else:
		channel = bot.get_channel(randomizerComp)
		await channel.send(f'Can\'t seem to do this, you don\'t have the proper role {ctx.message.author.mention}')
		
@bot.command(name='rankingNuz', help='Calculates the person\'s randomizer success value')
async def update_rank_players_in_Nuzlocke(ctx, *, arg: str):
	l = False
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == role_id:
			l = True
			break
	if l == True:
		channel = bot.get_channel(randomizerComp)
		await ctx.message.add_reaction(emoji_cool)
		arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
		await channel.send(arguments)		
		teen = arg.split(" ", 1)
		s = rb.rankAllRandomizerFormula(teen[0])
		p = 1
		channel2 = bot.get_channel(randomizerPublic)
		for help in s:
			if help != '':
				await channel.send('```' + 'In place #' + str(p) + '```')
				await channel.send('```' + help + '```')
				await channel2.send('```' + 'In place #' + str(p) + '```')
				await channel2.send('```' + help + '```')
				p += 1
	else:
		channel = bot.get_channel(randomizerComp)
		await channel.send(f'Can\'t seem to do this, you don\'t have the proper role {ctx.message.author.mention}')
		
@bot.command(name='trackLookups', help='Tracks the number of lookups a person has done')
async def check_num_Lookups_in_Nuzlocke(ctx, arg: str):
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji_cool)
	arguments = f'The message I will be outputting will be from {ctx.message.author.mention} with the arguments {arg}'
	await channel.send(arguments)
	for myguild in bot.guilds:
		if myguild.name == GUILD:
			break
	members = myguild.members
	validmembers = []
	for member in members:
		for role in member.roles:
			if role.id == role_id:
				validmembers.append(member.name)
	s = rb.lookupRefSheet(arg, validmembers)
	t = s.split("\n")
	t.pop(len(t)-1)
	channel2 = bot.get_channel(randomizerPublic)
	alternate = True
	for help in t:
		if alternate == True:
			await channel.send('```' + ' ' + '# of Lookups done by player' + ' ' + help + '```')
			await channel2.send('```' + ' ' + '# of Lookups done by player' + ' ' + help + '```')
			alternate = False
		else:
			await channel.send('```' + help + '```')
			await channel2.send('```' + help + '```')
			alternate = True

@bot.command(name='ping', help='Ping the bot to text name')
async def ping(ctx):
    await ctx.send(ctx.author.mention)
    #print("debug: " + dir(ctx.author))
	
@bot.command(name='kamalPain', help='Show Kamal\'s pain')
async def ping(ctx):
	await ctx.message.add_reaction(emoji_cool)
	await ctx.send("https://youtu.be/6F7VwTzf7vQ?t=3108")

	
@bot.command(name='99')
async def nine_nine(ctx):
    brooklyn_99_quotes = [
        'I\'m the human form of the 💯 emoji.',
        'Bingpot!',
        (
            'Cool. Cool cool cool cool cool cool cool, '
            'no doubt no doubt no doubt no doubt.'
        ),
    ]

    response = random.choice(brooklyn_99_quotes)
    await ctx.send(response)	
	

@bot.command(name='catchemlackin')
async def catch_em_Lackin(ctx, *, arg: str):
	for guild in bot.guilds:
		if guild.name == GUILD:
			break
			
	emoji = guild.emojis[0]
	await ctx.message.add_reaction(emoji)
	t = arg.split(" ")
	print(len(t))
	await ctx.send(emoji)
	l = False
	global rl
	for role in ctx.message.author.roles:
		if role.id == my_role:
			l = True
			break
	if rl == True or l == True:
		for mem in guild.members:
			print(mem.name)
			for x in t:
				if x.casefold() in mem.name.casefold():
					#await ctx.send(f'{mem.mention}')
					await mem.create_dm()
					await mem.dm_channel.send('Aye yo, you fuckin dumbass, next time you use TTS in my server again I\'ll grow a body just to beat your ass, comprende?')
					#await ctx.author.create_dm()
					#await ctx.author.dm_channel.send('Wake ya ass up')
	else:
		await ctx.message.author.create_dm()
		await ctx.message.author.dm_channel.send('Wake ya own ass up, fucking dumbass, you ain\'t my boss.')
	
	
@bot.command(name='switch', help='Turns the bot off')
async def switch(ctx):
	for guild in bot.guilds:
		if guild.name == GUILD:
			break
	global rl	
	emoji = guild.emojis[1]
	print(rl)
	l = False
	await ctx.message.add_reaction(emoji)
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == my_role:
			l = True
			break
	if l == True:
		rl = not rl
		print(rl)
		await ctx.send(f'I guess I gotta listen to da boss')
	else:
		await ctx.send(f'You think this is a game?')
		
@bot.command(name='wheelPunishments', help='Gives the current link to the wheel of Punishments')
async def wheel_Punishments(ctx):
	await ctx.message.add_reaction(emoji_cool)
	s = rb.getPunishmentSuggestionsWheelLink()
	await ctx.send('```' + s[0] + '```')
	
@bot.command(name='off', help='Turns the bot off')
async def shutdown(ctx):
	for guild in bot.guilds:
		if guild.name == GUILD:
			break
			
	emoji = guild.emojis[1]
	
	l = False
	channel = bot.get_channel(randomizerComp)
	await ctx.message.add_reaction(emoji)
	print(ctx.message.author.roles)
	for role in ctx.message.author.roles:
		if role.id == my_role:
			l = True
			break
	if l == True:
		await ctx.bot.logout()
	else:
		await ctx.send(f'You don\'t have the guts, young shounen.')
	
	
@bot.command(name='cycleProfile', help='Cycles profile pic for discord HAS A TIMER OF FIVE MINS, PLS DONT SPAM')
async def cycleProfile(ctx):
	global timed
	global start_timer
	global stop_timer
	emoji = ctx.guild.emojis[1]
	if timed == True:
		await ctx.message.add_reaction(emoji)
		s = sb.cycleImages()
		await ctx.send(file=discord.File(s))
		with open(s, 'rb') as f:
			newicon = f.read()
			await ctx.guild.edit(icon=newicon)
			await ctx.message.add_reaction(emoji_cool)
		timed = False
		start_timer = time.perf_counter()
		#print(s)
	else:
		stop_timer = time.perf_counter()
		dif = stop_timer - start_timer
		if dif > 300:
			changeTimed()
			await ctx.send('Give it 5 seconds, and I\'ll be ready to go again.')
		else:
			await ctx.message.add_reaction(emoji_uncool)
			await ctx.send('gotta wait a few, let the cycle cool down')


@bot.command(name='sweetPrince', help='Good Night With That Shit, Made to order from the bot')
async def goodNight(ctx):
	emoji = ctx.guild.emojis[1]
	await ctx.message.add_reaction(emoji)
	await ctx.send(file=discord.File('thebotgotheat/goodnite.png'))
			
@bot.command(name='botSpittin', help='Bot reaction images')
async def theBotGotHeat(ctx):
	ran = random.randint(0,2)
	
@bot.command(name='toggleLookupMarker', help='Toggles recording for lookups')
async def toggle_Lookups(ctx):
	emoji = ctx.guild.emojis[1]
	changeMarkerForLookups()
	await ctx.send('It has been done')
		
def changeTimed():
	global timed
	timed = True
	
def changeMarkerForLookups():
	global LookupMarker
	if LookupMarker == True:
		LookupMarker = False
	else:
		LookupMarker = True
	print(LookupMarker)
	
	
bot.run(token)